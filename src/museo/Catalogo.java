package museo;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

public class Catalogo {
    private ArrayList<Oggetto> listaOggetti;

    public Catalogo() {
        this.listaOggetti=new ArrayList<>();
    }

    public ArrayList<Oggetto> getListaOggetti() {
        return listaOggetti;
    }

    public void leggiFile(String f){
        File inputFile = new File(f);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(inputFile));

            while (buffer.ready()) {
                creaOggetto(buffer.readLine());
            }
            buffer.close();
        }catch (IOException e){
            System.err.println(e.getMessage());
        }

    }

    private boolean creaOggetto(String line){
        StringTokenizer token = new StringTokenizer(line,"\t");
        String codice;
        int anno;
        String descrizione;
        String nomeSala;

        codice=token.nextToken();
        anno=Integer.parseInt(token.nextToken());
        descrizione=token.nextToken();
        nomeSala=token.nextToken();

        return listaOggetti.add(new Oggetto(codice,anno,descrizione,nomeSala));
    }

    @Override
    public String toString() {
        return "Catalogo{" +
                "listaOggetti=" + listaOggetti +
                '}';
    }
}
