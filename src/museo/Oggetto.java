package museo;

public class Oggetto implements Comparable<Oggetto>{
    private String codiceIdentificativo;
    private int anno;
    private String descrizione;
    private String sala;

    public Oggetto(String codiceIdentificativo, int anno, String descrizione, String sala) {
        this.codiceIdentificativo = codiceIdentificativo;
        this.anno = anno;
        this.descrizione = descrizione;
        this.sala = sala;
    }

    public String getSala() {
        return sala;
    }

    public String getCodiceIdentificativo() {
        return codiceIdentificativo;
    }

    public int getAnno() {
        return anno;
    }

    @Override
    public int compareTo(Oggetto o) {
        int diff=0;
        diff = this.anno-o.getAnno();
        if(diff==0){
            diff=Integer.parseInt(String.valueOf(codiceIdentificativo)) - Integer.parseInt(String.valueOf(o.getCodiceIdentificativo()));
        }
        return diff;
    }

    @Override
    public String toString() {
        return codiceIdentificativo +"\t"+anno+"\t"+descrizione+"\n";
    }
}
