package museo;

import java.util.ArrayList;
import java.util.Collections;

public class Sala {
    ArrayList<Oggetto> oggetti;
    private String nome;
    private int numPezzi;

    public Sala(String nome) {
        this.nome = nome;
        this.numPezzi=0;
        this.oggetti = new ArrayList<>();
    }

    public void addOggetto(Oggetto o){
        if(o.getSala().equals(nome)){
            oggetti.add(o);
            numPezzi++;
        }
    }

    public String getNome() {
        return nome;
    }

    public ArrayList<Oggetto> getOggetti() {
        return oggetti;
    }

    public void stampaPezziOrdinati(){
        Collections.sort(oggetti);
        System.out.println(this);
    }

    public void stampaPezziSecolo(int secolo){
        int secolomax = secolo+100;
        for (Oggetto o:oggetti
        ) {
            if(o.getAnno() >= secolo && o.getAnno()<=secolomax){
                System.out.println(o);
            }
        }
    }

    public void stampanumPezzi(){
        System.out.println(nome +": "+numPezzi+"\n");
    }

    @Override
    public String toString() {
        return nome+"\n"
                +"--------------------------\n"+
                oggetti +"\n";
    }
}
