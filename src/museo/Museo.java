package museo;

import java.util.ArrayList;
import java.util.Collections;

public class Museo {
    private String nome;
    private Catalogo catalogo;
    private ArrayList<Sala> sale;

    public Museo(String nome) {
        this.nome = nome;
        this.sale = new ArrayList<>();
        inizializzaSale();
    }

    public void caricaCatalogo(String file){
        catalogo = new Catalogo();
        catalogo.leggiFile(file);
        aggiungiOggettiAlleSale();
    }


    private void inizializzaSale(){
        sale.add(new Sala("Il Salone Medioevale"));
        sale.add(new Sala("Il Salone Rinascimentale"));
        sale.add(new Sala("La Sala dell'Inquisizione"));
        sale.add(new Sala("La Sala delle Scienze Moderne"));
        sale.add(new Sala("La Sala delle Tecnologie Moderne"));
    }

    private void aggiungiOggettiAlleSale(){
        ArrayList<Oggetto> oggetti;
        oggetti = catalogo.getListaOggetti();

        for (Sala s: sale
             ) {
            for (Oggetto o: oggetti
                 ) {
                s.addOggetto(o);
            }
        }

    }

    public void stampaPezziOrdinati(){
        for (Sala s:sale
             ) {
            s.stampaPezziOrdinati();
        }
    }

    public void stampaPezziDellaSala(String sala){
        for (Sala s:sale
             ) {
            if(s.getNome().equals(sala)){
                System.out.println(s);
            }
        }
    }

    public void stampaPezziSecolo(int secolo){
        for (Sala s:sale
             ) {
            s.stampaPezziSecolo(secolo);
        }
    }

    public void stampapezziSalaeSecolo(String nomeSala,int secolo){
        for (Sala s:sale
             ) {
            if(s.getNome().equals(nomeSala)){
                s.stampaPezziSecolo(secolo);
            }
        }
    }

    public void stampaNumpezziperSala(){
        for (Sala s:sale
             ) {
            s.stampanumPezzi();
        }
    }

    @Override
    public String toString() {
        return nome + "\n"
                +"------------------------\n"+
                sale;
    }
}
