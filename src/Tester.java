import museo.Museo;

public class Tester {
    public static void main(String[] args) {
        Museo museo = new Museo("museo");
        museo.caricaCatalogo("./risorse/catalogo1");
        museo.stampaPezziDellaSala("Il Salone Medioevale");
    }
}
